# -*- coding: utf-8 -*-
"""
Config
"""
import os


class Config(object):
    """
    Enviroment Production
    """
    SECRET_KEY = 'my_secret_key'
    DEBUG = False


class DevelopmentConfig(Config):
    """
    Enviroment Development
    """
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'mysql://root:123456@0.0.0.0:8000/firefighter_db'
    SQLALCHEMY_DATABASE_URI_TEST = os.environ.get('DATABASE_URL') or \
        'mysql://root:123456@0.0.0.0:8000/firefighter_test_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
