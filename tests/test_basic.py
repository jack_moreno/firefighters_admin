# -*- coding: utf-8 -*-
import unittest
from app import APP, DB
from app.mod_security.models import Profile, User


class TestBasic(unittest.TestCase):
    """ Test basic is use like a template
    """

    def setUp(self):
        """ Create database
        Create tables to tests
        """
        APP.config['SQLALCHEMY_DATABASE_URI'] = APP.config["SQLALCHEMY_DATABASE_URI_TEST"]
        self.app = APP.test_client()
        # with APP.app_context():
        # DB.drop_all()

    def tearDown(self):
        pass

    """
    # TESTS
    """

    def test_start_db(self):
        DB.create_all()

    def test_create_profile(self):
        result = Profile("Admin", "").save()
        self.assertEqual(result, True)

    def test_create_user(self):
        result = User("admin@gmail.com", "123456", 1).save()
        self.assertEqual(result, True)

    def test_end_db(self):
        # DB.drop_all()
        pass


if __name__ == "__main__":
    unittest.main()
