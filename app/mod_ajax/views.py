# -*- coding: utf-8 -*-
from flask import Blueprint, request, jsonify
from app.mod_security.models import Ubigeo

MOD_AJAX = Blueprint('ajax', __name__, url_prefix='/ajax')


@MOD_AJAX.route("/province", methods=['POST'])
def province():
    department = request.form['id_department']
    data = Ubigeo.get_province(department)
    result = []
    for item in data:
        result.append({'id': item.province_code, 'title': item.name})
    return jsonify(result)


@MOD_AJAX.route("/district", methods=['POST'])
def district():
    department = request.form['id_department']
    province = request.form['id_province']
    data = Ubigeo.get_district(department, province)
    result = []
    for item in data:
        result.append({'id': item.id, 'title': item.name})
    return jsonify(result)
