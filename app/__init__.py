"""
Description: this is project represent the admin
"""
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import DevelopmentConfig
from app.mod_security.forms import LoginForm

APP = Flask(__name__)
APP.config.from_object(DevelopmentConfig)
DB = SQLAlchemy(APP)
MIGRATE = Migrate(APP, DB)


@APP.route('/')
def index():
    """
    Login
    """
    form = LoginForm(request.form)
    return render_template('index.html', form=form)


from app.mod_security.views import MOD_SECURITY
from app.mod_ajax.views import MOD_AJAX
APP.register_blueprint(MOD_SECURITY)
APP.register_blueprint(MOD_AJAX)

if __name__ == 'app':
    # APP.run()
    DB.init_app(APP)
    with APP.app_context():
        # crea las tablas que no existen
        DB.create_all()
