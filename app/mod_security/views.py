# -*- coding: utf-8 -*-
"""
VIEW: Module security
"""
from flask import Blueprint, request, render_template, g
from flask import redirect, url_for, flash, session
from app.mod_security import forms, models, decorators

MOD_SECURITY = Blueprint('security', __name__, url_prefix='/security')


@MOD_SECURITY.before_request
def before_request():
    g.auth = None
    if 'auth_id' in session:
        g.auth = models.User.get_by_id(session['auth_id'])


@MOD_SECURITY.route('/', methods=['GET', 'POST'])
def index():
    """
    DEF: Login
    """
    form = forms.LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        email = form.email.data
        password = form.password.data
        user = models.User.get_by_email(email=email)
        if user is not None and models.User.verify_password(user, password):
            message = 'Bienvenido {}'.format(email)
            session['auth_id'] = user.id
            flash(message, 'success')
            return redirect(url_for('security.dashboard'))
        flash('Usuario o contraseña no válidas!', 'danger')
    return render_template('index.html', form=form)


@MOD_SECURITY.route('/dashboard', methods=['GET'])
@decorators.requires_login_auth
def dashboard():
    """
    DEF: Dashboard
    """
    return render_template('/security/dashboard.html')


@MOD_SECURITY.route('/profile')
@decorators.requires_login_auth
def profile_list():
    title = 'Listado de Perfiles'
    page = request.args.get('page', 1)
    data = models.Profile.get_all(page)
    return render_template('/security/profile_list.html',
                           data=data, title=title)


@MOD_SECURITY.route('/profile/create', methods=['GET', 'POST'])
@decorators.requires_login_auth
def profile_create():
    title = "Ingresar Perfil"
    form = forms.ProfileForm(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        description = form.description.data
        data = models.Profile(title, description).save()
        if data:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.profile_list'))
        else:
            flash('Ocurrio algún error', 'warning')
    return render_template('/security/profile_create.html', form=form,
                           title=title)


@MOD_SECURITY.route('/profile/edit/<int:pk>', methods=['GET', 'POST'])
@decorators.requires_login_auth
def profile_edit(pk):
    title = 'Editar Perfil'
    profile = models.Profile.get_by_id(pk)
    form = forms.ProfileForm(request.form, profile)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        description = form.description.data
        data = models.Profile(title, description).update(pk)
        if data:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.profile_list'))
        else:
            flash('Ocurrio algún error', 'warning')
    return render_template('/security/profile_create.html', form=form,
                           title=title)


@MOD_SECURITY.route('/profile/delete/<int:pk>', methods=['GET'])
@decorators.requires_login_auth
def profile_delete(pk):
    models.Profile.delete(pk)
    flash('El registro se elimino satisfactoriamente.', 'success')
    return redirect(url_for('security.profile_list'))


@MOD_SECURITY.route('/profile/permission/<int:pk>', methods=['GET', 'POST'])
@decorators.requires_login_auth
def profile_permission(pk):
    title = 'Editar Permisos'
    form = forms.ProfilePermissionForm(request.form)
    # get all profile permissions
    profile_permissions = models.ProfilePermission.get_by_profile(pk)
    # profile_permissions_id = [p.permission_id for p in profile_permissions]
    # get all the available permits
    all_permissions = models.Permission.get_active()
    # set all available permissions in the form
    form.permission_id.choices = [(p.id, p.title) for p in all_permissions]
    if request.method == 'POST' and form.validate():
        permission_id = form.permission_id.data

        # deactive all
        models.ProfilePermission.delete_by_profile(pk)
        if permission_id is None:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.profile_list'))
        # activate permissions
        data = 0
        for p in permission_id:
            models.ProfilePermission(pk, p).save()
            data = data + 1
        if data == len(permission_id):
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.profile_list'))
        else:
            flash('Ocurrio algún error', 'warning')

    # set all profile permissions in the list of permits
    form.permission_id.data = [(p.permission_id) for p in profile_permissions]
    return render_template('/security/profile_permission.html', form=form,
                           title=title)


@MOD_SECURITY.route('/user')
@decorators.requires_login_auth
def user_list():
    title = 'Listado de Usuarios'
    page = request.args.get('page', 1)
    data = models.User.get_all(page)
    return render_template('/security/user_list.html', data=data, title=title)


@MOD_SECURITY.route('/user/create', methods=['GET', 'POST'])
@decorators.requires_login_auth
def user_create():
    title = "Ingresar Usuario"
    form = forms.UserForm(request.form)
    form.profile_id.choices = [(p.id, p.title) for p in models.Profile.get_active()]
    if request.method == 'POST' and form.validate():
        email = form.email.data
        password = form.password.data
        profile_id = form.profile_id.data
        data = models.User(email, password, profile_id).save()
        if data:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.user_list'))
        else:
            flash('Ocurrio algún error', 'warning')
    return render_template('/security/user_create.html', form=form,
                           title=title)


@MOD_SECURITY.route('/user/edit/<int:pk>', methods=['GET', 'POST'])
@decorators.requires_login_auth
def user_edit(pk):
    title = 'Editar Usuario'
    user = models.User.get_by_id(pk)
    form = forms.UserForm(request.form, user)
    if form.password.data == '':
        form.password.data = '******'
    form.profile_id.choices = [(p.id, p.title) for p in models.Profile.get_active()]
    if request.method == 'POST' and form.validate():
        email = form.email.data
        password = form.password.data
        profile_id = form.profile_id.data
        data = models.User(email, password, profile_id).update(pk)
        if data:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.user_list'))
        else:
            flash('Ocurrio algún error', 'warning')
    return render_template('/security/user_create.html', form=form,
                           title=title)


@MOD_SECURITY.route('/user/delete/<int:pk>', methods=['GET'])
@decorators.requires_login_auth
def user_delete(pk):
    models.User.delete(pk)
    flash('El registro se elimino satisfactoriamente.', 'success')
    return redirect(url_for('security.user_list'))


@MOD_SECURITY.route('/permission')
@decorators.requires_login_auth
def permission_list():
    title = 'Listado de Permisos'
    page = request.args.get('page', 1)
    data = models.Permission.get_all(page)
    return render_template('/security/permission_list.html', data=data, title=title)


@MOD_SECURITY.route('/permission/create', methods=['GET', 'POST'])
@decorators.requires_login_auth
def permission_create():
    title = "Ingresar Permiso"
    form = forms.PermissionForm(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        url = form.url.data
        data = models.Permission(title, url).save()
        if data:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.permission_list'))
        else:
            flash('Ocurrio algún error', 'warning')
    return render_template('/security/permission_create.html', form=form,
                           title=title)



@MOD_SECURITY.route('/permission/edit/<int:pk>', methods=['GET', 'POST'])
@decorators.requires_login_auth
def permission_edit(pk):
    title = 'Editar Permiso'
    permission = models.Permission.get_by_id(pk)
    form = forms.PermissionForm(request.form, permission)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        url = form.url.data
        data = models.Permission(title, url).update(pk)
        if data:
            flash('Los datos se guardaron satisfactoriamente.', 'success')
            return redirect(url_for('security.permission_list'))
        else:
            flash('Ocurrio algún error', 'warning')
    return render_template('/security/permission_create.html', form=form,
                           title=title)


@MOD_SECURITY.route('/permission/delete/<int:pk>', methods=['GET'])
@decorators.requires_login_auth
def permission_delete(pk):
    models.Permission.delete(pk)
    flash('El registro se elimino satisfactoriamente.', 'success')
    return redirect(url_for('security.permission_list'))


@MOD_SECURITY.route('/logout')
def logout():
    if 'auth_id' in session:
        session.pop('auth_id')
    return redirect(url_for('index'))
