# -*- coding: utf-8 -*-
from app import DB
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash

PER_PAGE = 30


class Base(DB.Model):
    __abstract__ = True
    id = DB.Column(DB.Integer, primary_key=True)
    status = DB.Column(DB.Enum('A', 'D', 'E'),
                       server_default='A',
                       nullable=False)
    register_date = DB.Column(DB.DateTime,
                              server_default=DB.func.current_timestamp(),
                              nullable=False)


class Profile(Base):
    __tablename__ = 'fire_profile'
    title = DB.Column(DB.String(50), nullable=False)
    description = DB.Column(DB.Text())
    user_fk = DB.relationship('User')

    def __init__(self, title, description):
        self.title = title
        self.description = description

    def get_active():
        return Profile.query.filter(Profile.status == 'A').all()

    def get_by_id(pk):
        return Profile.query.filter_by(id=pk).first()

    def save(self):
        try:
            data = Profile(self.title, self.description)
            DB.session.add(data)
            DB.session.commit()
            return True
        except Exception:
            return False

    def update(self, pk):
        try:
            data = Profile.get_by_id(pk)
            if data is None:
                return False
            data.title = self.title
            data.description = self.description
            DB.session.commit()
            return True
        except Exception:
            return False

    def get_all(page=1):
        data = Profile.query.filter(Profile.status == 'A')
        return data.paginate(page, PER_PAGE)

    def delete(pk):
        data = Profile.query.filter_by(id=pk).first()
        data.status = 'E'
        DB.session.commit()
        return True


class User(Base):
    __tablename__ = 'fire_user'
    email = DB.Column(DB.String(150), nullable=False)
    password = DB.Column(DB.String(100), nullable=False)
    profile_id = DB.Column(DB.Integer, DB.ForeignKey('fire_profile.id'))
    profile = DB.relationship('Profile')

    def __init__(self, email, password, profile_id):
        self.email = email
        if password == '******':
            self.password = password
        else:
            self.password = self.create_password(password)
        self.profile_id = profile_id

    def create_password(self, password):
        return generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password, password)

    def get_by_id(pk):
        return User.query.filter_by(id=pk).first()

    def get_by_email(email):
        return User.query.filter_by(email=email).first()

    def get_all(page=1):
        data = data = User.query.filter(User.status == 'A')
        return data.paginate(page, PER_PAGE)

    def save(self):
        try:
            data = User(self.email, self.password, self.profile_id)
            DB.session.add(data)
            DB.session.commit()
            return True
        except Exception:
            return False

    def update(self, pk):
        try:
            data = User.get_by_id(pk)
            if data is None:
                return False
            data.email = self.email
            if self.password != '******':
                data.password = self.password
            data.profile_id = self.profile_id
            DB.session.commit()
            return True
        except Exception:
            return False

    def delete(pk):
        data = User.query.filter_by(id=pk).first()
        data.status = 'E'
        DB.session.commit()
        return True


class Ubigeo(Base):
    __tablename__ = 'fire_ubigeo'
    department_code = DB.Column(DB.Integer)
    province_code = DB.Column(DB.Integer)
    district_code = DB.Column(DB.Integer)
    name = DB.Column(DB.String(50), nullable=False)

    def get_by_id(pk):
        return Ubigeo.query.filter_by(id=pk).first()

    def get_department():
        return Ubigeo.query.filter_by(status='A', province_code=0).all()

    def get_province(department_id):
        data = Ubigeo.query.filter(Ubigeo.status == 'A',
                                   Ubigeo.province_code > 0,
                                   Ubigeo.department_code == department_id,
                                   Ubigeo.district_code == 0)
        return data

    def get_district(department_id, province_id):
        data = Ubigeo.query.add_columns(
            Ubigeo.id,
            Ubigeo.name, Ubigeo.status
        ).filter(Ubigeo.status != 'E',
                 Ubigeo.department_code == department_id,
                 Ubigeo.province_code == province_id,
                 Ubigeo.district_code > 0)
        return data


class Permission(Base):
    __tablename__ = 'fire_permission'
    title = DB.Column(DB.String(100), nullable=False)
    url = DB.Column(DB.String(250), nullable=False)

    def __init__(self, title, url):
        self.title = title
        self.url = url

    def get_by_id(pk):
        return Permission.query.filter_by(id=pk).first()

    def get_active():
        return Permission.query.filter(Permission.status == 'A').all()

    def get_all(page=1):
        data = data = Permission.query.filter(Permission.status == 'A')
        return data.paginate(page, PER_PAGE)

    def save(self):
        try:
            data = Permission(self.title, self.url)
            DB.session.add(data)
            DB.session.commit()
            return True
        except Exception:
            return False

    def update(self, pk):
        try:
            data = Permission.get_by_id(pk)
            if data is None:
                return False
            data.title = self.title
            data.url = self.url
            DB.session.commit()
            return True
        except Exception:
            return False

    def delete(pk):
        data = Permission.query.filter_by(id=pk).first()
        data.status = 'E'
        DB.session.commit()
        return True


class ProfilePermission(Base):
    __tablename__ = 'fire_profile_permission'
    profile_id = DB.Column(DB.Integer, DB.ForeignKey('fire_profile.id'), nullable=False)
    permission_id = DB.Column(DB.Integer, DB.ForeignKey('fire_permission.id'), nullable=False)

    def __init__(self, profile_id, permission_id):
        self.profile_id = profile_id
        self.permission_id = permission_id

    def get_by_profile(profile_id):
        return ProfilePermission.query.filter_by(profile_id=profile_id,
                                                 status="A").all()

    def save(self):
        try:
            data = ProfilePermission(self.profile_id, self.permission_id)
            DB.session.add(data)
            DB.session.commit()
            return True
        except Exception:
            return False

    def delete_by_profile(profile_id):
        profile_permissions = ProfilePermission.query.filter_by(profile_id=profile_id).all()
        for permission in profile_permissions:
            DB.session.delete(permission)
        DB.session.commit()
        return True
