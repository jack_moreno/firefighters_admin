# -*- coding: utf-8 -*-
"""
SECURITY: FORMS
"""
from wtforms import Form, StringField, PasswordField, TextAreaField, SelectField
from wtforms import SelectMultipleField, validators, widgets
from wtforms.fields.html5 import EmailField


class LoginForm(Form):
    """
        Login Form
    """
    email = EmailField('Correo', [
        validators.Required(message='El correo es requerido.'),
        validators.Length(min=5, max=30, message='El correo no es valido.'),
        validators.Email(message='El formato del correo no es correcto.'),
    ], render_kw={'placeholder': 'CORREO'})

    password = PasswordField('Password', [
        validators.Required(message='La contraseña es requerida.')],
        render_kw={'placeholder': 'CONTRASEÑA'})


class ProfileForm(Form):
    """
        Profile Form
    """
    title = StringField('Título', [
        validators.Required(message='El título es requerido.'),
    ], render_kw={'placeholder': 'Título'})

    description = TextAreaField('Descripción',
                                render_kw={'placeholder': 'Descripción'})


class PermissionForm(Form):
    """
        Permission Form
    """
    title = StringField('Título', [
        validators.Required(message='El título es requerido.'),
    ], render_kw={'placeholder': 'Título'})

    url = StringField('Url', [
        validators.Required(message='Url es requerida.'),
    ], render_kw={'placeholder': 'Url'})


class UserForm(Form):
    """
        User Form
    """
    email = EmailField('Correo', [
        validators.Required(message='El correo es requerido.'),
        validators.Length(min=5, max=30, message='El correo no es valido.'),
        validators.Email(message='El formato del correo no es correcto.'),
    ], render_kw={'placeholder': 'CORREO'})

    password = PasswordField('Password', [
        validators.Required(message='La contraseña es requerida.')],
        render_kw={'placeholder': 'CONTRASEÑA'})

    profile_id = SelectField('Perfil', [
        validators.Required(
            message='La selección de un perfil es requerida.')
    ], coerce=int)


class ProfilePermissionForm(Form):
    """
        Profile Permission Form
    """
    permission_id = SelectMultipleField(
        'Permisos',
        widget=widgets.ListWidget(html_tag='ul', prefix_label=False),
        option_widget=widgets.CheckboxInput(),
        coerce=int
    )
