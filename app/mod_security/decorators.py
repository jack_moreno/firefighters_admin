from functools import wraps
from flask import g, flash, redirect, url_for, request


def requires_login_auth(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.auth is None:
            flash('Necesitas loguearte para acceder a esta página.')
            return redirect(url_for('index', next=request.path))
        return f(*args, **kwargs)
    return decorated_function
