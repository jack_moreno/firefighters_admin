# -*- coding: utf-8 -*-
import datetime


class Date():
    """
        Utils: date formats
    """
    def __init__(self, date):
        self.date = date

    def db(self):
        if self.date == "" or self.date is None:
            return None
        self.date = str(self.date)
        dt = datetime.datetime.strptime(self.date, '%d/%m/%Y')
        return '{0}-{1}-{2}'.format(dt.year, dt.month, dt.day)

    def client(self):
        if self.date == "" or self.date is None:
            return None
        self.date = str(self.date)
        dt = datetime.datetime.strptime(self.date, '%Y-%m-%d')
        return '{0}/{1}/{2}'.format(dt.day, dt.month, dt.year)

    def clientdt(self):
        if self.date == "" or self.date is None:
            return None
        self.date = str(self.date)
        dt = datetime.datetime.strptime(self.date, '%Y-%m-%d %H:%M:%S')
        return '{0}/{1}/{2} {3}:{4}:{5}'.format(dt.day, dt.month,
                                                dt.year, dt.hour,
                                                dt.minute, dt.second)
